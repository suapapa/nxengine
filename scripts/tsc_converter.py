#!/usr/bin/python

import os
import sys
import pdb

'''
=== TSC Converter (cross-platform) ===
=========   By Carrot Lord   =========
'''

hextuple = ("\x00","\x01","\x02","\x03","\x04","\x05","\x06","\x07","\x08","\x09",
            "\x0a","\x0b","\x0c","\x0d","\x0e","\x0f",
            "\x10","\x11","\x12","\x13","\x14","\x15","\x16","\x17","\x18","\x19",
            "\x1a","\x1b","\x1c","\x1d","\x1e","\x1f",
            "\x20","\x21","\x22","\x23","\x24","\x25","\x26","\x27","\x28","\x29",
            "\x2a","\x2b","\x2c","\x2d","\x2e","\x2f",
            "\x30","\x31","\x32","\x33","\x34","\x35","\x36","\x37","\x38","\x39",
            "\x3a","\x3b","\x3c","\x3d","\x3e","\x3f",
            "\x40","\x41","\x42","\x43","\x44","\x45","\x46","\x47","\x48","\x49",
            "\x4a","\x4b","\x4c","\x4d","\x4e","\x4f",
            "\x50","\x51","\x52","\x53","\x54","\x55","\x56","\x57","\x58","\x59",
            "\x5a","\x5b","\x5c","\x5d","\x5e","\x5f",
            "\x60","\x61","\x62","\x63","\x64","\x65","\x66","\x67","\x68","\x69",
            "\x6a","\x6b","\x6c","\x6d","\x6e","\x6f",
            "\x70","\x71","\x72","\x73","\x74","\x75","\x76","\x77","\x78","\x79",
            "\x7a","\x7b","\x7c","\x7d","\x7e","\x7f",
            "\x80","\x81","\x82","\x83","\x84","\x85","\x86","\x87","\x88","\x89",
            "\x8a","\x8b","\x8c","\x8d","\x8e","\x8f",
            "\x90","\x91","\x92","\x93","\x94","\x95","\x96","\x97","\x98","\x99",
            "\x9a","\x9b","\x9c","\x9d","\x9e","\x9f",
            "\xa0","\xa1","\xa2","\xa3","\xa4","\xa5","\xa6","\xa7","\xa8","\xa9",
            "\xaa","\xab","\xac","\xad","\xae","\xaf",
            "\xb0","\xb1","\xb2","\xb3","\xb4","\xb5","\xb6","\xb7","\xb8","\xb9",
            "\xba","\xbb","\xbc","\xbd","\xbe","\xbf",
            "\xc0","\xc1","\xc2","\xc3","\xc4","\xc5","\xc6","\xc7","\xc8","\xc9",
            "\xca","\xcb","\xcc","\xcd","\xce","\xcf",
            "\xd0","\xd1","\xd2","\xd3","\xd4","\xd5","\xd6","\xd7","\xd8","\xd9",
            "\xda","\xdb","\xdc","\xdd","\xde","\xdf",
            "\xe0","\xe1","\xe2","\xe3","\xe4","\xe5","\xe6","\xe7","\xe8","\xe9",
            "\xea","\xeb","\xec","\xed","\xee","\xef",
            "\xf0","\xf1","\xf2","\xf3","\xf4","\xf5","\xf6","\xf7","\xf8","\xf9",
            "\xfa","\xfb","\xfc","\xfd","\xfe","\xff")

def display2():
    print "---------------"
    print ""

def error(msg):
    display2()
    print "Error. " + msg
    raw_input("Exiting program...")
    sys.exit()

def pause():
    raw_input("")

def tsc_to_txt(inputFileName, outputFileName):
    fpTSC = open(inputFileName, "rb")
    fpTXT = open(outputFileName, "wb")

    cipher = get_cipher(fpTSC)

    offtxt = 0
    offtsc = 0
    while (1):
        fpTSC.seek(offtsc,0)
        fpTXT.seek(offtxt,0)
        charTSC = fpTSC.read(1)
        if (len(charTSC) != 1):
            #print "Reached end of file."
            break
        else:
            fpTSC.seek(offtsc,0)
            if ord(charTSC)==cipher: #if the character is the cipher itself
                fpTXT.write(hextuple[cipher])
            else:
                fpTXT.write(hextuple[ord(charTSC) - cipher]) #otherwise, use the cipher.
            #1. ord() will convert a char -> integer ASCII value.
            #2. subtract 0x20 to decode each char
            #3. hextuple it to convert to hex string
            #4. finally, write it to the file.
            offtxt += 1
            offtsc += 1 #increment both offsets by appropriate amounts.
        #endwhile
    return

def txt_to_tsc(inputFileName, outputFileName):
    fpTXT = open(inputFileName, "rb")
    fpTSC = open(outputFileName, "wb")

    cipher, middlelocation = make_cipher(fpTXT)

    offtxt = 0
    offtsc = 0
    while (1):
        fpTSC.seek(offtsc,0)
        fpTXT.seek(offtxt,0)
        charTXT = fpTXT.read(1)
        if (len(charTXT) != 1):
            print "Reached end of file."
            break
        else:
            fpTXT.seek(offtxt,0)
            if offtxt == middlelocation:
                fpTSC.write(hextuple[cipher])
            else:
                if ord(charTXT) + cipher > 255:
                    #error("Hextuple is out of range.\nTrying to add " + hex(ord(charTXT)) + " with cipher " + str(cipher))
                    fpTSC.write("\xff")
                else:
                    fpTSC.write(hextuple[ord(charTXT) + cipher])
            #1. ord() will convert a char -> integer ASCII value.
            #2. add 0x20 to encode each char
            #3. hextuple it to convert to hex string
            #4. finally, write it to the file.
            offtxt += 1
            offtsc += 1 #increment both offsets by appropriate amounts.
        #endwhile
    return

def get_cipher(fpTSC):
    #fpTSC is already opened
    #note that the code1 must always be 0D
    nldict = {} #create an empty newline dictionary.
    offtsc = 0
    while (1):
        fpTSC.seek(offtsc,0)
        charTSC = fpTSC.read(1)
        char2TSC = fpTSC.read(1) #seeked position will automatically move forward
        if (len(charTSC) != 1) or (len(char2TSC) != 1):
            break #break the while if end of file is reached.
        code1 = ord(charTSC)
        code2 = ord(char2TSC)
        if (code1 - code2) == 3: #if the difference is 3, i.e. a newline character
            if nldict.has_key(code1): #if the dictionary contains the key already
                nldict[code1] += 1 #increment that key's value.
            else:
                nldict[code1] = 1 #set starting count.
        offtsc += 1  #increment offset.
        #endwhile
    listofkeys = nldict.keys()
    topvalue = 0
    topkey = "null"
    for keyx in listofkeys:
        if nldict[keyx] > topvalue:
            topkey = keyx
            topvalue = nldict[keyx]
    topkey -= 0x0D #0D compensates for ASCII 0D
    return topkey #the topkey is the cipher, in decimal.

def make_cipher(fpTXT):
    charTEST = "-"
    while len(charTEST) == 1:
        charTEST = fpTXT.read(1) #find the end of the file.
    endfile = fpTXT.tell() #get end of file
    endfile = int(round(endfile/2)) #divide by 2, then round.
    fpTXT.seek(endfile,0)
    tuple1 = (ord(fpTXT.read(1)),endfile)
    return tuple1 #this is the [cipher in decimal,endfilelocation]

if __name__ == "__main__":
    from optparse import OptionParser
    parser = OptionParser("usage: %prog [options] <TSC or TXT>")
    parser.add_option("-d", "--decode",
                      action="store_true", default=False,
                      help="Explictly decode TSC to TXT")
    parser.add_option("-e", "--encode",
                      action="store_true", default=False,
                      help="Explictly encode TXT to TSC")
    parser.add_option("-o", "--output", type="string",
                      help="Output name")

    (opts, args) = parser.parse_args()
    inputFiles = args
    modeDec = opts.decode
    modeEnc = opts.encode
    outputFile = opts.output

    # Option error handing...
    if modeDec and modeEnc:
        print("ERR! select only one action, encode or decode!")
        exit(-1)

    if (not modeDec) and (not modeEnc):
        print("ERR! should select a action, encode or decode!")
        exit(-1)

    if not inputFiles:
        print("ERR! must specify input file!")
        exit(-1)

    if outputFile and not (len(inputFiles) == 1):
        print("ERR! coundn't specify a output file name when multiple input given!")
        exit (-1)

    # Do convering...
    for inputFile in inputFiles:
        if modeEnc:
            if not outputFile:
                outputFile = inputFile[:inputFile.rfind('.')] + ".txt"
            print "Encoding %s to %s..."%(inputFile, outputFile)
            tsc_to_txt(inputFile, outputFile)

        elif modeDec:
            if not outputFile:
                outputFile = inputFile[:inputFile.rfind('.')] + ".tsc"
            print "Decoding %s to %s..."%(inputFile, outputFile)
            txt_to_tsc(inputFile, outputFile)

    # All done
    exit(0)
