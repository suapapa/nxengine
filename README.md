# NXEngine

A nearly-complete open-source clone/engine-rewrite of the classic
jump-and-run platformer Doukutsu Monogatari (also known as Cave Story).

This is an *unofficial* repository which started from original NXEngine
sourcecode v1.0.0.3. 

# How to build

    $ scons -j6

# Reference sites

- [Official NXEngine site](http://nxengine.sourceforge.net/)
- [Cave Story (Doukutsu Monogatary), A Tribute Site](http://www.cavestory.org/)
- [Cave Story in Wikipedia](http://en.wikipedia.org/wiki/Cave_Story)
